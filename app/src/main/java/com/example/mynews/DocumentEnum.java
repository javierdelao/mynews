package com.example.mynews;

public enum DocumentEnum {

    NEWS("news");

    private String documentPath;

    DocumentEnum(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getDocumentPath() {
        return documentPath;
    }
}

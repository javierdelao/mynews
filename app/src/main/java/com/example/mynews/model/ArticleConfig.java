package com.example.mynews.model;

public class ArticleConfig {
    private String descripcion;
    private boolean estado;

    public ArticleConfig() {
    }

    public ArticleConfig(String descripcion, Boolean estado) {
        this.descripcion = descripcion;
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}

package com.example.mynews;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mynews.model.User;
import com.example.mynews.services.FirebaseAuthManager;
import com.google.android.material.snackbar.Snackbar;

public class LoginActivity extends AppCompatActivity {

    private EditText email;

    private EditText password;

    private Button enterLoginBtn;

    private LoginActivity thisActivity;

    private FirebaseAuthManager authManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        createViewItems();
        createModelItems();
        redirectIfLogin();
    }


    public void createViewItems(){
        enterLoginBtn = findViewById(R.id.loginEnterBtn);
        email = findViewById(R.id.loginEmail);
        password = findViewById(R.id.loginPassword);
    }

    public void createModelItems() {
        thisActivity = this;
        authManager = new FirebaseAuthManager();
    }

    public void login(View view) {
        String textEmail = email.getText().toString();
        String textPassword = password.getText().toString();
        System.out.println(textEmail);
        System.out.println(textPassword);
        if(textEmail.isEmpty() || textPassword.isEmpty()){
            Snackbar.make(view, "xedxd", Snackbar.LENGTH_LONG).show();
        }
        authManager.login(thisActivity, email.getText().toString(), password.getText().toString(), task -> {
            if (task.isSuccessful()) {
                User user = authManager.getCurrentUser();
                if(user == null) {
                    Snackbar.make(view, "1 login error "+textEmail+" "+textPassword, Snackbar.LENGTH_LONG).show();
                } else {
                    redirectToNewsList();
                }
            } else {
                Snackbar.make(view, "1 login error "+textEmail+" "+textPassword, Snackbar.LENGTH_LONG).show();
            }
        });

    }

    public void redirectToNewsList(){
        Intent intent = new Intent(this, ItemListActivity.class);
        startActivity(intent);
        finish();
    }

    public void redirectIfLogin() {
        if(authManager.isLogin()) {
            redirectToNewsList();
        }
    }
}

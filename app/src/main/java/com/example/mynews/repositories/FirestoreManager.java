package com.example.mynews.repositories;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public class FirestoreManager {
    //Firestore collections
    public static final String FS_COLLECTION_VERSION = "version";
    public static final String FS_COLLECTION_CLIENTS = "clients";
    public static final String FS_COLLECTION_NEWS = "news";

    //Firestore documents
    public static final String FS_DOCUMENT_INFO = "info";

    //Firestore fields
    public static final String FS_FIELD_CODE = "code";

    private FirebaseFirestore db;

    public FirestoreManager() {
        this.db = FirebaseFirestore.getInstance();
    }

    public void getCollection(String collectionPath, OnCompleteListener<QuerySnapshot> responseHandler){
        db.collection(collectionPath)
                .get()
                .addOnCompleteListener(responseHandler);
    }

    public void getCollection(String collectionPath, String categoryFilter, OnCompleteListener<QuerySnapshot> responseHandler){
            db.collection(collectionPath).whereArrayContains("categories",categoryFilter)
                    .get()
                    .addOnCompleteListener(responseHandler);

    }

    public void getDocument(String collectionPath, String documentKey, OnCompleteListener<DocumentSnapshot> responseHandler) {
        db.collection(collectionPath)
                .document(documentKey)
                .get()
                .addOnCompleteListener(responseHandler);
    }

    public void getDocumentByTitle(String collectionPath, String atributePath,String value, OnCompleteListener<QuerySnapshot> responseHandler) {
        db.collection(collectionPath)
                .whereEqualTo(atributePath,value)
                .get()
                .addOnCompleteListener(responseHandler);
    }

    public void setValue(String collectionPath, String documentKey, Object objectToSave, OnSuccessListener<Void> successHandler, OnFailureListener failureHandler){
        db.collection(collectionPath)
                .document(documentKey)
                .set(objectToSave)
                .addOnSuccessListener(successHandler)
                .addOnFailureListener(failureHandler);
    }
}

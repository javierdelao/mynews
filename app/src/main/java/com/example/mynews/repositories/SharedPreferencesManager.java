package com.example.mynews.repositories;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class SharedPreferencesManager {

    public static final String SHARED_PREFERENCES = "preferences";
    public static final String KEY_CLIENTS = "clients";
    public static final String KEY_CATEGORIES = "categories";

    public static void savePreference(ContextWrapper context, String key, Object value) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();
        String objectString = gson.toJson(value);
        editor.putString(key, objectString);
        editor.commit();
    }

    public static void savePreference(ContextWrapper context, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void savePreference(ContextWrapper context, String key, int value) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static Object getObjectPreference(ContextWrapper context, String key, Class clazz) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        String jsonObject = preferences.getString(key, "");
        if(jsonObject.isEmpty()) {
            return null;
        }
        Gson gson = new Gson();
        return gson.fromJson(jsonObject, clazz);
    }

    public static String getStringPreference(ContextWrapper context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

    public static int getIntPreference(ContextWrapper context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return preferences.getInt(key, 0);
    }
}

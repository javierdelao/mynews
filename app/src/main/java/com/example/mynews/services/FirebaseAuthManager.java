package com.example.mynews.services;

import android.app.Activity;

import com.example.mynews.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseAuthManager {

    private FirebaseAuth auth;

    public FirebaseAuthManager() {
        this.auth = FirebaseAuth.getInstance();
    }

    public boolean isLogin() {
        return this.auth.getCurrentUser() != null;
    }

    public User getCurrentUser() {
        FirebaseUser currentUser = this.auth.getCurrentUser();
        if(currentUser == null) {
            return null;
        }
        return new User(currentUser.getDisplayName(), currentUser.getEmail());
    }

    public void login(Activity context, String email, String password, OnCompleteListener<AuthResult> responseHandler) {
        if(email.isEmpty() || password.isEmpty()) {
            return;
        }
        this.auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(context, responseHandler);
    }

    public void logout() {
        this.auth.signOut();
    }

}

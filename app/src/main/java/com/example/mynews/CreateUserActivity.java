package com.example.mynews;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.mynews.model.User;
import com.example.mynews.repositories.FirestoreManager;
import com.example.mynews.services.FirebaseAuthManager;
import com.google.android.material.snackbar.Snackbar;

public class CreateUserActivity extends AppCompatActivity {


    private FirestoreManager firestoreManager;

    private CreateUserActivity thisActivity;

    private FirebaseAuthManager authManager;

    private EditText name;
    private EditText email;
    private Switch enabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.generalToolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Usuarios");
        setSupportActionBar(toolbar);
        createModelItems();
        createViewItems();
    }

    public void createModelItems() {
        thisActivity = this;
        this.firestoreManager = new FirestoreManager();
        authManager = new FirebaseAuthManager();
    }

    public void createViewItems() {
        name = findViewById(R.id.add_article_title);
        email = findViewById(R.id.add_article_summary);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        //getMenuInflater().inflate(R.menu.add_article_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void createUser(View view) {
        String tmpName = this.name.getText().toString();
        String tmpEmail = this.email.getText().toString();
        User user = new User();
        user.setName(tmpName);
        user.setEmail(tmpEmail);
        save(user);
    }

    public void save(User user) {
        firestoreManager.setValue(
                FirestoreManager.FS_COLLECTION_CLIENTS,
                user.getEmail(),
                user,
                aVoid -> {
                    Snackbar.make(thisActivity.name, "Done", Snackbar.LENGTH_LONG).show();
                    goToUserList();
                },
                e -> {
                    Snackbar.make(thisActivity.name, e.getMessage(), Snackbar.LENGTH_LONG).show();
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.add_article_logout){
            authManager.logout();
            goToUserList();
        } else if(id == R.id.add_article_go_to_list){
            finish();
        }
        return  super.onOptionsItemSelected(item);
    }

    public void goToUserList(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}

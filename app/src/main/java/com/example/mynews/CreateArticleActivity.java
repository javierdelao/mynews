package com.example.mynews;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

import com.example.mynews.model.Article;
import com.example.mynews.repositories.FirestoreManager;
import com.example.mynews.services.FirebaseAuthManager;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Date;

public class CreateArticleActivity extends AppCompatActivity {


    private FirestoreManager firestoreManager;

    private CreateArticleActivity thisActivity;

    private FirebaseAuthManager authManager;

    private EditText title;
    private EditText summary;
    private EditText body;

    private Switch cienciaSwitch;

    private Switch politicaSwitch;

    private Switch musicaSwitch;

    private Switch arteSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_article);
        Toolbar toolbar = (Toolbar) findViewById(R.id.generalToolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Noticias");
        setSupportActionBar(toolbar);
        createModelItems();
        createViewItems();
    }

    public void createModelItems() {
        thisActivity = this;
        this.firestoreManager = new FirestoreManager();
        authManager = new FirebaseAuthManager();
    }

    public void createViewItems() {
        title  = findViewById(R.id.add_article_title);
        summary  = findViewById(R.id.add_article_summary);
        body  = findViewById(R.id.add_article_body);
        cienciaSwitch = findViewById(R.id.addArticleCienciaCategory);
        politicaSwitch = findViewById(R.id.addArticlePoliticaCategory);
        musicaSwitch = findViewById(R.id.addArticleMusicaCategory);
        arteSwitch = findViewById(R.id.addArticleArteCategory);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.add_article_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void createArticle(View view) {
        String tmpTitle = this.title.getText().toString();
        String tmpSummary = this.summary.getText().toString();
        String tmpBody = this.body.getText().toString();
        Article article = new Article();
        article.setTitle(tmpTitle);
        article.setSummary(tmpSummary);
        article.setBody(tmpBody);
        article.setPublicationDate(new Date());
        article.setCategories(new ArrayList<>());
        System.out.println(cienciaSwitch.isChecked());
        if(cienciaSwitch.isChecked()){
            article.getCategories().add("Ciencia");
        }
        if(politicaSwitch.isChecked()){
            article.getCategories().add("Politica");
        }
        if(musicaSwitch.isChecked()){
            article.getCategories().add("Musica");
        }
        if(arteSwitch.isChecked()){
            article.getCategories().add("Arte");
        }
        saveClientToFirestore(article);
    }

    public void saveClientToFirestore(Article article) {
        firestoreManager.setValue(
                FirestoreManager.FS_COLLECTION_NEWS,
                article.getId(),
                article,
                aVoid -> {
                    Snackbar.make(thisActivity.title, "xdxd", Snackbar.LENGTH_LONG).show();
                    goToArticleList();
                },
                e -> {
                    Snackbar.make(thisActivity.title, e.getMessage(), Snackbar.LENGTH_LONG).show();
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.add_article_logout){
            authManager.logout();
            goToArticleList();
        } else if(id == R.id.add_article_go_to_list){
            finish();
        } else if(id == R.id.add_article_settings){
            Intent intent = new Intent(this, ArticleCategoryFilter.class);
            startActivity(intent);
        }
        return  super.onOptionsItemSelected(item);
    }

    public void goToArticleList(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}

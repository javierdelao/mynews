package com.example.mynews;

import android.os.Bundle;
import android.view.View;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mynews.model.*;
import com.example.mynews.repositories.FirestoreManager;
import com.example.mynews.repositories.SharedPreferencesManager;

import java.util.ArrayList;

public class ArticleCategoryFilter extends AppCompatActivity {
    private ArticleCategoryFilter thisActivity;

    private FirestoreManager firestoreManager;
    private ArrayList<ArticleConfig> _ArticleConfig;
    private ArticleConfig _ArticleConfigC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_category_filter);
        thisActivity = this;
        firestoreManager = new FirestoreManager();
        createModelItems();
    }

    public void createModelItems() {
        Switch aSwitch;
        aSwitch = (Switch) findViewById(R.id.swciencia);
        aSwitch.setChecked(Boolean.parseBoolean(SharedPreferencesManager.getStringPreference(thisActivity,"ciencia")));
        aSwitch = (Switch) findViewById(R.id.swarte);
        aSwitch.setChecked(Boolean.parseBoolean(SharedPreferencesManager.getStringPreference(thisActivity,"arte")));
        aSwitch = (Switch) findViewById(R.id.swmusica);
        aSwitch.setChecked(Boolean.parseBoolean(SharedPreferencesManager.getStringPreference(thisActivity,"musica")));
        aSwitch = (Switch) findViewById(R.id.swpolitica);
        aSwitch.setChecked(Boolean.parseBoolean(SharedPreferencesManager.getStringPreference(thisActivity,"politica")));

    }

    public void swNewsConfig(View view) {
        Switch aSwitch;
        ArticleConfig _ArticleConfig = new ArticleConfig();
        switch (view.getId()) {
            case R.id.swciencia:
                aSwitch = (Switch) findViewById(R.id.swciencia);
                SharedPreferencesManager.savePreference(thisActivity,"ciencia",Boolean.toString(aSwitch.isChecked()) );
                break;
            case R.id.swarte:
                aSwitch = (Switch) findViewById(R.id.swarte);
                SharedPreferencesManager.savePreference(thisActivity,"arte",Boolean.toString(aSwitch.isChecked()) );
                break;
            case R.id.swmusica:
                aSwitch = (Switch) findViewById(R.id.swmusica);
                SharedPreferencesManager.savePreference(thisActivity,"musica",Boolean.toString(aSwitch.isChecked()) );
                break;
            case R.id.swpolitica:
                aSwitch = (Switch) findViewById(R.id.swpolitica);
                SharedPreferencesManager.savePreference(thisActivity,"politica",Boolean.toString(aSwitch.isChecked()) );
                break;

        }
    }

}

package com.example.mynews;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import com.example.mynews.model.Article;
import com.example.mynews.repositories.FirestoreManager;
import com.example.mynews.repositories.SharedPreferencesManager;
import com.example.mynews.services.FirebaseAuthManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ItemDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ItemListActivity extends AppCompatActivity {


    private List<Article>articles;

    private FrameLayout frameLayout;
    private FloatingActionButton fab;
    private FirestoreManager firestoreManager;

    private ItemListActivity thisActivity;

    private FirebaseAuthManager authManager;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.generalToolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Noticias");
        setSupportActionBar(toolbar);
        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
        View recyclerView = findViewById(R.id.item_list);
        assert recyclerView != null;
        createModelItems();
        createViewItems();
        loadData(recyclerView);

    }

    @Override
    protected void onResume() {
        super.onResume();
        View recyclerView = findViewById(R.id.item_list);
        assert recyclerView != null;
        loadData(recyclerView);
    }

    public void loadData(View recyclerView){
        articles = new ArrayList<>();
        List<String> categoriesFilter = getCategoryFilter();
        if(categoriesFilter.size()>0){
            for(String categoryFilter: categoriesFilter){
                firestoreManager.getCollection(
                        DocumentEnum.NEWS.getDocumentPath(),
                        categoryFilter,
                        task -> {
                            if(task.isSuccessful()){
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Article article = new Article();
                                    article.setId(document.getString("id"));
                                    Boolean exists = false;
                                    for(Article articleTmp: articles){
                                        if(articleTmp.getId().equals(article.getId())){
                                            exists = true;
                                        }
                                    }
                                    if(!exists){
                                        article.setTitle(document.getString("title"));
                                        article.setSummary(document.getString("summary"));
                                        article.setBody(document.getString("body"));
                                        article.setPublicationDate(document.getDate("publicationDate"));
                                        articles.add(article);
                                    }
                                }
                                setupRecyclerView((RecyclerView) recyclerView);
                            }
                        });
            }
        } else {
            firestoreManager.getCollection(
                    DocumentEnum.NEWS.getDocumentPath(),
                    task -> {
                        if(task.isSuccessful()){
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Article article = new Article();
                                article.setId(document.getString("id"));
                                Boolean exists = false;
                                for(Article articleTmp: articles){
                                    if(articleTmp != null && articleTmp.getId() != null && article.getId()!=null){
                                        if(articleTmp.getId().equals(article.getId())){
                                            exists = true;
                                        }
                                    }
                                }
                                if(!exists){
                                    article.setTitle(document.getString("title"));
                                    article.setSummary(document.getString("summary"));
                                    article.setBody(document.getString("body"));
                                    article.setPublicationDate(document.getDate("publicationDate"));
                                    articles.add(article);
                                }
                            }
                            setupRecyclerView((RecyclerView) recyclerView);
                        }
                    });
        }
        setupRecyclerView((RecyclerView) recyclerView);
    }

    public List<String> getCategoryFilter(){
        List<String> categoriesFilter=new ArrayList<>();
      String cienciaResponse = (String)SharedPreferencesManager.getObjectPreference(thisActivity,"ciencia",String.class);
      if(cienciaResponse != null && Boolean.TRUE.equals(Boolean.parseBoolean(cienciaResponse))){
          categoriesFilter.add("Ciencia");
      }
        String politicaResponse = (String)SharedPreferencesManager.getObjectPreference(thisActivity,"politica",String.class);
        if(politicaResponse != null && Boolean.TRUE.equals(Boolean.parseBoolean(politicaResponse))){
            categoriesFilter.add("Politica");
        }
        String arteResponse = (String)SharedPreferencesManager.getObjectPreference(thisActivity,"arte",String.class);
        if(arteResponse != null && Boolean.TRUE.equals(Boolean.parseBoolean(arteResponse))){
            categoriesFilter.add("Arte");
        }
        String musicaResponse = (String)SharedPreferencesManager.getObjectPreference(thisActivity,"musica",String.class);
        if(musicaResponse != null && Boolean.TRUE.equals(Boolean.parseBoolean(musicaResponse))){
            categoriesFilter.add("Musica");
        }
      return categoriesFilter;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.commonmenus,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.logout){
            authManager.logout();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else if(id == R.id.addArticle){
            Intent intent = new Intent(this, CreateArticleActivity.class);
            startActivity(intent);
        } else if (id == R.id.addUser) {
            Intent intent = new Intent(this, CreateUserActivity.class);
            startActivity(intent);
        } else if(id == R.id.add_article_settings){
            Intent intent = new Intent(this, ArticleCategoryFilter.class);
            startActivity(intent);
        }
        return  super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(this, articles, mTwoPane));
    }

    public void createViewItems() {
        frameLayout  = findViewById(R.id.frameLayout);
    }

    public void createModelItems() {
        thisActivity = this;
        this.firestoreManager = new FirestoreManager();
        authManager = new FirebaseAuthManager();
        articles = new ArrayList<>();
    }

    public static class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final ItemListActivity mParentActivity;
        private final List<Article> articles;
        private final boolean mTwoPane;
        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Article item = (Article) view.getTag();
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putString("contentTitle", item.getTitle());
                    arguments.putString("contentBody", item.getBody());
                    arguments.putString("contentSummary", item.getSummary());
                    ItemDetailFragment fragment = new ItemDetailFragment();
                    fragment.setArguments(arguments);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.item_detail_container, fragment)
                            .commit();
                } else {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, ItemDetailActivity.class);
                    intent.putExtra("contentTitle", item.getTitle());
                    intent.putExtra("contentSummary", item.getSummary());
                    intent.putExtra("contentBody", item.getBody());
                    context.startActivity(intent);
                }
            }
        };

        SimpleItemRecyclerViewAdapter(ItemListActivity parent,
                                      List<Article> articles,
                                      boolean twoPane) {

            this.articles = articles;
            mParentActivity = parent;
            mTwoPane = twoPane;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mContentView.setText(articles.get(position).getTitle());
           // holder.mContentView.setText(articles.get(position).getBody());

            holder.itemView.setTag(articles.get(position));
            holder.itemView.setOnClickListener(mOnClickListener);
        }

        @Override
        public int getItemCount() {
            return articles.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final TextView mIdView;
            final TextView mContentView;

            ViewHolder(View view) {
                super(view);
                mIdView = (TextView) view.findViewById(R.id.id_text);
                mContentView = (TextView) view.findViewById(R.id.content);
            }
        }
    }
}
